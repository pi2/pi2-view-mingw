# PI2 View MINGW

This repository contains package scripts for PI2 View to build under MSYS2 and a script to create a standalone distribution.

## Preparing the build environment

Install MSYS2 following the [official instructions](https://www.msys2.org/)!

## Making the MSYS2 package

Build the package with:

```bash
$ cd ${package-name}
$ MINGW_INSTALLS=mingw64 makepkg-mingw -sf
```

(use `MINGW_INSTALLS=mingw32` to make a 32 bit package)

After that you could install (not necessary) the freshly built package with:

```bash
$ pacman -U ${package-name}*.pkg.tar.xz
```

## Creating a standalone distribution

Use [`make-standalone`](make-standalone) to create a standalone distribution from an MSYS2 package:

```bash
$ ../make-standalone ${package-name}*.pkg.tar.xz
```
